package drivers.mobile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

import enums.TipoOS;
import enums.TipoRetorno;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import planilha.Planilha;

public class DriverMobile {

	private static AndroidDriver<?> driverAndroid = null;
	private static IOSDriver<?> driverIOS = null;
	private static Planilha planilha = new Planilha();
	private static DesiredCapabilities capabilities = null;
	private static String caminhoApk = "src" + File.separator + "test" + File.separator + "resources" + File.separator + "armazenador" + File.separator;
	
	public static void iniciarAndroid(String identificadorSetadoNaPlanilhaDeTestes, boolean nativo) {
		try {
			iniciarDriverAndroid(identificadorSetadoNaPlanilhaDeTestes, nativo);
			validarContexto(nativo, driverAndroid);
			driverAndroid.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void iniciarIOS(String identificadorSetadoNaPlanilhaDeTestes, boolean nativo) {
		try {
			iniciarDriverIOS(identificadorSetadoNaPlanilhaDeTestes, nativo);
			validarContexto(nativo, driverIOS);
			driverIOS.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static IOSDriver<?> getDriverIOS() {
		return driverIOS;
	}

	public static AndroidDriver<?> getDriverAndroid() {
		return driverAndroid;
	}

	public static void FinalizarDriver() {
		if (driverAndroid != null) {
			driverAndroid.quit();
			driverAndroid = null;
		} else if (driverIOS != null) {
			driverIOS.quit();
			driverIOS = null;
		}
	}
	
	@SuppressWarnings("rawtypes")
	private static void iniciarDriverAndroid(String identificadorSetadoNaPlanilhaDeTestes, boolean nativo) throws MalformedURLException, Exception {
			driverAndroid = new AndroidDriver(new URL("http://"+ planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.URLAPPIUM)+ "/wd/hub"), definirCapabilitiesAndroid(identificadorSetadoNaPlanilhaDeTestes, nativo));
	}
	
	@SuppressWarnings("rawtypes")
	private static void iniciarDriverIOS(String identificadorSetadoNaPlanilhaDeTestes, boolean nativo) throws MalformedURLException, Exception {
			driverIOS = new IOSDriver(new URL("http://"+ planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.URLAPPIUM)+ "/wd/hub"), definirCapabilitesIOS(identificadorSetadoNaPlanilhaDeTestes, nativo));
	}
	
	@SuppressWarnings("rawtypes")
	private static void validarContexto(boolean nativo, AppiumDriver driver) {
		if (!nativo) driver.context("WEBVIEW");
	}
	
	private static DesiredCapabilities definirCapabilitesIOS(String identificadorSetadoNaPlanilhaDeTestes, boolean nativo) {
		capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, TipoOS.IOS);
		capabilities.setCapability(MobileCapabilityType.APP, obterCaminhoApk(identificadorSetadoNaPlanilhaDeTestes));
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.DEVICEID));
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.VERSAODOSO));
		if (!nativo) {	capabilities.setCapability(MobileCapabilityType.AUTO_WEBVIEW, true); }
		
		try {
			if (planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.APPWAITACTIVITY) != null || !planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.APPWAITACTIVITY).matches(" ") || planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.APPWAITACTIVITY).matches("")) {
				capabilities.setCapability("appWaitActivity", planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.APPWAITACTIVITY));
			}
		} catch (NullPointerException e) { //TODO
		}
		
		return capabilities;
	}

	private static DesiredCapabilities definirCapabilitiesAndroid(String identificadorSetadoNaPlanilhaDeTestes, boolean nativo) {
		capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, TipoOS.ANDROID);
		capabilities.setCapability(MobileCapabilityType.APP, obterCaminhoApk(identificadorSetadoNaPlanilhaDeTestes));
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.DEVICEID));
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.VERSAODOSO));
		if (!nativo) {	capabilities.setCapability(MobileCapabilityType.AUTO_WEBVIEW, true); }
		try {
			if (planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.APPWAITACTIVITY) != null || !planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.APPWAITACTIVITY).matches(" ") || planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.APPWAITACTIVITY).matches("")) {
				capabilities.setCapability("appWaitActivity", planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.APPWAITACTIVITY));
			}
		} catch (NullPointerException e) { //TODO
		}
		return capabilities;
	}
	
	private static String obterCaminhoApk(String identificadorSetadoNaPlanilhaDeTestes) {
		try {
			caminhoApk = new File(caminhoApk +  planilha.retornarElementoDaPlanilha(identificadorSetadoNaPlanilhaDeTestes, TipoRetorno.NOMEAPK)).getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return caminhoApk;
	}
}