package thread;

import java.util.ArrayList;
import java.util.List;

import drivers.mobile.DriverMobile;
import drivers.web.DriverWeb;
import enums.Browser;
import enums.TipoRetorno;
import planilha.Planilha;

public class ConfigurarThread {
	private List<Thread> th = new ArrayList<>();
	private ConfiguracoesCucumber configuracoesCucumber = new ConfiguracoesCucumber();
	// -------------------------------------------------------------------//
	private Planilha planilha = null;
	private String pluginReportJunit = null;// arquivoDeConfiguracao.getCaminhoXMLJunit();
	private String pluginReportJsonCucumber = null;// arquivoDeConfiguracao.getCaminhoJsonCucumber();
	private String caminhoDasFeatures = null;// arquivoDeConfiguracao.getCaminhoDasFeatures();
	private String pacoteSteps = null;// arquivoDeConfiguracao.getPacoteSteps();
	// ------------------------------------------------------------------//

	/**
	 * @param identificadorConfiguracaoCucumberPlanilhaDeTestes
	 */
	protected ConfigurarThread(String identificadorConfiguracaoCucumberPlanilhaDeTestes) {
		planilha = new Planilha();
		pacoteSteps = planilha.retornarElementoDaPlanilha(identificadorConfiguracaoCucumberPlanilhaDeTestes,
				TipoRetorno.PACOTESTEPS);
		caminhoDasFeatures = planilha.retornarElementoDaPlanilha(identificadorConfiguracaoCucumberPlanilhaDeTestes,
				TipoRetorno.CAMINHODASFEATURES);
		pluginReportJunit = planilha.retornarElementoDaPlanilha(identificadorConfiguracaoCucumberPlanilhaDeTestes,
				TipoRetorno.CAMINHOARQUIVORELATORIOXML);
		pluginReportJsonCucumber = planilha.retornarElementoDaPlanilha(
				identificadorConfiguracaoCucumberPlanilhaDeTestes, TipoRetorno.CAMINHOARQUIVORELATORIOJSON);
	}

	/**
	 * @param tags
	 * @param finalizarDriver
	 * @throws InterruptedException
	 */
	protected void setarConfiguracoes(List<String> tags, boolean finalizarDriver, Browser navegadorDeExecucao, String caminhoDoDriver,
			boolean maximizarJanelaDoNavegador, boolean executarSemInterfaceGrafica) throws InterruptedException {
		executor(pluginReportJunit, pluginReportJsonCucumber, tags, caminhoDasFeatures, pacoteSteps, finalizarDriver,
				navegadorDeExecucao, caminhoDoDriver, maximizarJanelaDoNavegador, executarSemInterfaceGrafica);
	}

	/**
	 * @param tags
	 * @param finalizarDriver
	 * @throws InterruptedException
	 */
	protected void setarConfiguracoes(List<String> tags, boolean finalizarDriver,
			List<String> identificadorDoAparelhoNaPlanilhaDeTestes, boolean nativo) throws InterruptedException {
		executor(pluginReportJunit, pluginReportJsonCucumber, tags, caminhoDasFeatures, pacoteSteps, finalizarDriver,
				identificadorDoAparelhoNaPlanilhaDeTestes, nativo);
	}

	private void executor(String pluginReportJunit, String pluginReportJsonCucumber, List<String> tags,
			String caminhoDasFeatures, String pacoteSteps, boolean finalizarDriver, Browser navegadorDeExecucao, String caminhoDoDriver,
			boolean maximizarJanelaDoNavegador, boolean executarSemInterfaceGrafica) throws InterruptedException {
		addThread(pluginReportJunit, pluginReportJsonCucumber, tags, caminhoDasFeatures, pacoteSteps, finalizarDriver,
				navegadorDeExecucao, caminhoDoDriver, maximizarJanelaDoNavegador, executarSemInterfaceGrafica);
		iniciarThread();
	}

	private void executor(String pluginReportJunit, String pluginReportJsonCucumber, List<String> tags,
			String caminhoDasFeatures, String pacoteSteps, boolean finalizarDriver,
			List<String> identificadorDoAparelhoNaPlanilhaDeTestes, boolean nativo) throws InterruptedException {
		addThread(pluginReportJunit, pluginReportJsonCucumber, tags, caminhoDasFeatures, pacoteSteps, finalizarDriver,
				identificadorDoAparelhoNaPlanilhaDeTestes, nativo);
		iniciarThread();
	}

	@SuppressWarnings("static-access")
	private void iniciarThread() throws InterruptedException {
		Thread a = null;
		for (Thread t : th) {
			a = t;
			t.start();
			t.sleep(3000);
		}
		while (a.isAlive()) {
		}
	}

	private void addThread(String pluginReportJunit, String pluginReportJsonCucumber, List<String> tags,
			String caminhoDasFeatures, String pacoteSteps, boolean finalizarDriver, Browser navegadorDeExecucao, String caminhoDoDriver,
			boolean maximizarJanelaDoNavegador, boolean executarSemInterfaceGrafica) {
			th.add(new Thread() {
				public void run() {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					DriverWeb.getDriver(navegadorDeExecucao, caminhoDoDriver, maximizarJanelaDoNavegador, executarSemInterfaceGrafica);
					configuracoesCucumber.configurarOpcoes(pluginReportJunit, pluginReportJsonCucumber, tags,
							caminhoDasFeatures, pacoteSteps);
					finalizarDriver(finalizarDriver);
				}
			});
		}

	private void addThread(String pluginReportJunit, String pluginReportJsonCucumber, List<String> tags,
			String caminhoDasFeatures, String pacoteSteps, boolean finalizarDriver,
			List<String> identificadorDoAparelhoNaPlanilhaDeTestes, boolean nativo) {
		for (String aparelho : identificadorDoAparelhoNaPlanilhaDeTestes) {
			th.add(new Thread() {
				public void run() {
					DriverMobile.iniciarAndroid(aparelho, nativo);
					configuracoesCucumber.configurarOpcoes(pluginReportJunit, pluginReportJsonCucumber, tags,
							caminhoDasFeatures, pacoteSteps);
					finalizarDriver(finalizarDriver);
				}

			});
		}
	}

	private void finalizarDriver(boolean finalizarDriver) {
		if (finalizarDriver) {
			DriverWeb.FinalizarDriver();
			DriverMobile.FinalizarDriver();
		}
	}
}