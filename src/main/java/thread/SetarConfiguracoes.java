package thread;

import java.util.List;

import enums.Browser;

public abstract class SetarConfiguracoes extends Exec{

	public void setarConfiguracoes(String identificadorConfiguracaoCucumberPlanilhaDeTestes, List<String> tags,
			boolean encerrarDrivers, Browser navegadorDeExecucao, String caminhoDoDriver, boolean maximizarJanelaDoNavegador, boolean executarSemInterfaceGrafica) {
		//Utilitaria utils = new Utilitaria();
		ConfigurarThread configurarThread = new ConfigurarThread(identificadorConfiguracaoCucumberPlanilhaDeTestes);
		try {
			configurarThread.setarConfiguracoes(tags, encerrarDrivers, navegadorDeExecucao, caminhoDoDriver, maximizarJanelaDoNavegador, executarSemInterfaceGrafica);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
//		utils.gerarEvidenciaWord(gerarEvidenciaWord, identificadorConfiguracaoCucumberPlanilhaDeTestes);
	}
	
	public void setarConfiguracoes(String identificadorConfiguracaoCucumberPlanilhaDeTestes, List<String> tags,
			boolean encerrarDrivers, List<String> identificadorDoAparelhoNaPlanilhaDeTestes, boolean nativo) {

		//Utilitaria utils = new Utilitaria();
		ConfigurarThread configurarThread = new ConfigurarThread(identificadorConfiguracaoCucumberPlanilhaDeTestes);

		try {
			configurarThread.setarConfiguracoes(tags, encerrarDrivers, identificadorDoAparelhoNaPlanilhaDeTestes,
					nativo);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
//		utils.gerarEvidenciaWord(gerarEvidenciaWord, identificadorConfiguracaoCucumberPlanilhaDeTestes);
	}
}