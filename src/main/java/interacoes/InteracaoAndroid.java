package interacoes;

import interfaces.mobile.android.IAcoesAPP;
import interfaces.mobile.android.IAcoesDevice;
import interfaces.mobile.android.IArrastar;
import interfaces.mobile.android.IClique;
import interfaces.mobile.android.ICombo;
import interfaces.mobile.android.IEscrever;
import interfaces.mobile.android.ILimpar;
import interfaces.mobile.android.IMover;
import interfaces.mobile.android.IObter;
import interfaces.mobile.android.IProcurar;
import interfaces.mobile.android.IUtils;
import interfaces.mobile.android.IValidacoesAndroid;

public interface InteracaoAndroid extends IAcoesAPP, IAcoesDevice, IArrastar, IClique, ICombo, IEscrever, ILimpar,
		IMover, IObter, IProcurar, IValidacoesAndroid, IUtils {

	String nomePlataformaDeExecucao = null;

}