package util;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;

import geradordeevidenciaword.DocumentoDeEvidencia;

public class Utilitaria {

	public static String escreverHoraMinutoSegundo() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH.mm.ss");
		LocalDateTime agora = LocalDateTime.now(ZoneId.systemDefault());
		String agoraFormatado = agora.format(formatter);
		return agoraFormatado;
	}

	public String escreverData() {
		LocalDate lcd = LocalDate.now(ZoneId.systemDefault());
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String data = lcd.format(formatter);
		return data;
	}

	public String escreverDataFutura(int dias) {
		LocalDate lcd = LocalDate.now(ZoneId.systemDefault());
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate data = lcd.plusDays(dias);
		return data.format(formatter).toString();
	}

	public String parsePdf(String pdf) throws IOException {
		PdfReader reader = new PdfReader(pdf);
		PdfReaderContentParser parser = new PdfReaderContentParser(reader);
		TextExtractionStrategy strategy = null;
		String texto = null;
		for (int i = 1; i <= reader.getNumberOfPages(); i++) {
			strategy = parser.processContent(i, new SimpleTextExtractionStrategy());
			texto = texto + " " + strategy.getResultantText();
		}
		reader.close();
		return texto;
	}

	public String parsePdfDois(String pdf) throws IOException {
		PdfReader reader = new PdfReader(pdf);
		PdfReaderContentParser parser = new PdfReaderContentParser(reader);
		TextExtractionStrategy strategy = null;
		String texto = null;
		for (int i = 1; i <= reader.getNumberOfPages(); i++) {
			strategy = parser.processContent(i, new SimpleTextExtractionStrategy());
			texto = texto + " " + strategy.getResultantText();
		}
		reader.close();
		return texto;
	}

	public void gerarEvidenciaWord(boolean gerar, String identificadorConfiguracaoCucumberPlanilha) {
		DocumentoDeEvidencia documentoDeEvidencia = new DocumentoDeEvidencia();
		if (gerar) {
			documentoDeEvidencia.gerar_evidencias(identificadorConfiguracaoCucumberPlanilha);
		}
	}

//	public void gerarPlanilhaExcelComGherkin(boolean gerar)
//			throws InvalidFormatException, IOException, InterruptedException {
//		PlanilhaDeTestesGherkin planilhaDeTestesGherkin = new PlanilhaDeTestesGherkin();
//		if (gerar) {
//			planilhaDeTestesGherkin.gerar();
//		}
//	}
}