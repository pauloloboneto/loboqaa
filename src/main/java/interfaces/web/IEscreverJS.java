package interfaces.web;

import java.time.LocalDateTime;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.mozilla.javascript.JavaScriptException;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;

import drivers.web.DriverWeb;

public interface IEscreverJS {
	Log logger = LogFactory.getLog(IEscreverJS.class);

	default void escreverJavascript(By elemento, String texto, String descricaoDoPasso) {
		try {
			logger.info("----" + descricaoDoPasso);
			logger.info("---- Massa utilizada: " + texto);
			JavascriptExecutor executor = (JavascriptExecutor) DriverWeb.getDriver();
			executor.executeScript("arguments[0].value=" + texto + ";", DriverWeb.getDriver().findElement(elemento));
		} catch (NoSuchElementException e) {
			logger.warn(" -- ERRO: elemento: '" + elemento + "' NAO encontrado.'");
			Assert.fail(LocalDateTime.now() + " -- NAO foi possivel localizar o elemento: '" + elemento + "' em tela.");
		} catch (TimeoutException e) {
			logger.warn(" -- ERRO: Tempo excedido para encontrar elemento: '" + elemento);
			Assert.fail(LocalDateTime.now() + " Tempo excedido para encontrar o elemento: '" + elemento + "' em tela.");
		} catch (ElementNotVisibleException e) {
			logger.warn(" -- ERRO: elemento: '" + elemento + "' NAO esta visivel na plataforma: '");
			Assert.fail(LocalDateTime.now() + " -- O elemento: " + elemento + "NAO esta visivel' em tela.");
		}catch(JavaScriptException e) {
			logger.warn(" -- ERRO: erro de Javascript ao tentar realizar acao no elemento: '" + elemento);
			Assert.fail(LocalDateTime.now() + " -- erro de Javascript ao tentar realizar acao no elemento: '" + elemento);
		}
	}
}