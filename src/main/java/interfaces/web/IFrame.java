package interfaces.web;

import java.time.LocalDateTime;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.TimeoutException;

import drivers.web.DriverWeb;

public interface IFrame {
	Log logger = LogFactory.getLog(IFrame.class);

	default void entrarFrame(String elemento, String descricaoDoPasso) {
		try {
			logger.info("----" + descricaoDoPasso);
			DriverWeb.getDriver().switchTo().frame(elemento);
		} catch (NoSuchElementException e) {
			logger.warn(" -- ERRO: frame: '" + elemento + "' NAO encontrado.'");
			Assert.fail(LocalDateTime.now() + " -- NAO foi possivel localizar o frame: '" + elemento + "' em tela.");
		} catch (TimeoutException e) {
			logger.warn(" -- ERRO: tempo excedido para encontrar frame: '" + elemento);
			Assert.fail(LocalDateTime.now() + " Tempo excedido para encontrar o frame: '" + elemento + "' em tela.");
		} catch (ElementNotVisibleException e) {
			logger.warn(" -- ERRO: elemento: '" + elemento + "' NAO esta visivel na plataforma: '");
			Assert.fail(LocalDateTime.now() + " -- O frame: " + elemento + "NAO esta visivel' em tela.");
		} catch (NoSuchFrameException e) {
			logger.warn(" -- ERRO: frame: '" + elemento + "' NAO encontrado.'");
			Assert.fail(LocalDateTime.now() + " -- NAO foi possivel localizar o frame: '" + elemento + "' em tela.");
		}
	}

	default void sairFrame(String descricaoDoPasso) {
		logger.info("----" + descricaoDoPasso);
		DriverWeb.getDriver().switchTo().defaultContent();
	}
}