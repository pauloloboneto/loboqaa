package interfaces.web;

import java.time.LocalDateTime;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;

import drivers.web.DriverWeb;

public interface IAlert {
	Log logger = LogFactory.getLog(IAlert.class);

	default void aceitarAlerta(String descricaoDoPasso) {
		try {
			logger.info("----" + descricaoDoPasso);
			Alert alert = DriverWeb.getDriver().switchTo().alert();
			alert.accept();
		} catch (NoAlertPresentException e) {
			logger.warn(" -- ERRO: NAO ha alerta presente na tela.");
			Assert.fail(LocalDateTime.now() + " NAO ha alerta presente na tela.");
		} catch (Exception e) {
			logger.warn(" -- ERRO: erro ao aceitar alerta:" + e.getMessage());
			Assert.fail(LocalDateTime.now() + "erro ao aceitar alerta: " + e.getMessage());
		}
	}

	default String obterTextoDoAlerta(String descricaoDoPasso) {
		String texto = null;
		try {
			logger.info("----" + descricaoDoPasso);
			Alert alert = DriverWeb.getDriver().switchTo().alert();
			texto = alert.getText();
		} catch (NoAlertPresentException e) {
			logger.warn(" -- ERRO: NAO ha alerta presente na tela.");
			Assert.fail(LocalDateTime.now() + " NAO ha alerta presente na tela.");
		} catch (Exception e) {
			logger.warn(" -- ERRO: erro ao obter texto do alerta:" + e.getMessage());
			Assert.fail(LocalDateTime.now() + "erro ao obter texto do alerta: " + e.getMessage());
		}
		return texto;
	}

	default void negarAlerta(String descricaoDoPasso) {
		try {
			logger.info("----" + descricaoDoPasso);
			Alert alert = DriverWeb.getDriver().switchTo().alert();
			alert.dismiss();
		} catch (NoAlertPresentException e) {
			logger.warn(" -- ERRO: NAO ha alerta presente na tela.");
			Assert.fail(LocalDateTime.now() + " NAO ha alerta presente na tela.");
		} catch (Exception e) {
			logger.warn(" -- ERRO: erro ao escrever no alerta:" + e.getMessage());
			Assert.fail(LocalDateTime.now() + "erro ao escrever no alerta: " + e.getMessage());
		}
	}

	default void escreverNoAlerta(String texto, String descricaoDoPasso) {
		try {
			logger.info("----" + descricaoDoPasso);
			logger.info("---- Massa utilizada: " + texto);
			Alert alert = DriverWeb.getDriver().switchTo().alert();
			alert.sendKeys(texto);
		} catch (Exception e) {
			logger.warn(" -- ERRO: erro ao escrever o texto: " + texto + " no alerta:" + e.getMessage());
			Assert.fail(LocalDateTime.now() + " erro ao escrever o texto: " + texto + " no alerta: " + e.getMessage());
		}
	}
}