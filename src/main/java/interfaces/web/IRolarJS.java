package interfaces.web;

import java.time.LocalDateTime;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.mozilla.javascript.JavaScriptException;
import org.openqa.selenium.JavascriptExecutor;

import drivers.web.DriverWeb;

public interface IRolarJS {
	Log logger = LogFactory.getLog(IRolarJS.class);

	default void rolarJavascript(int quantidade, String descricaoDoPasso) {
		try {
			logger.info("----" + descricaoDoPasso);
			JavascriptExecutor jse = (JavascriptExecutor) DriverWeb.getDriver();
			jse.executeScript("window.scrollBy(0, " + quantidade + ")");
		} catch (JavaScriptException e) {
			logger.warn(" -- ERRO: erro de Javascript ao tentar realizar acao de rolagem.");
			Assert.fail(LocalDateTime.now() + " -- erro de Javascript ao tentar realizar acao de rolagem.");
		}
	}
}