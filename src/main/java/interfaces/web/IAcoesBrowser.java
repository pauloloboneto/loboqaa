package interfaces.web;

import java.time.LocalDateTime;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.TimeoutException;

import drivers.web.DriverWeb;

public interface IAcoesBrowser {
	Log logger = LogFactory.getLog(IAcoesBrowser.class);

	default void abrirUrl(String url, String descricaoDoPasso) {
		try {
			logger.info("----" + descricaoDoPasso);
			DriverWeb.getDriver().get(url);
		} catch (Exception e) {
			logger.warn(" -- ERRO: erro ao abrir url: " + url);
			Assert.fail(LocalDateTime.now() + "erro ao abrir url: " + url);
		}
	}

	default void navegarUrl(String url, String descricaoDoPasso) {
		try {
			logger.info("----" + descricaoDoPasso);
			DriverWeb.getDriver().navigate().to(url);
		} catch (Exception e) {
			logger.warn(" -- ERRO: erro ao navegar para a url: " + url);
			Assert.fail(LocalDateTime.now() + "erro ao navegar para a url: " + url);
		}
	}

	default void trocarJanela(Integer indice, String descricaoDoPasso) {
		try {
			logger.info("----" + descricaoDoPasso);
			Set<String> handles = DriverWeb.getDriver().getWindowHandles();
			Object[] it = handles.toArray();
			DriverWeb.getDriver().switchTo().window((String) it[indice]);
		} catch (NoSuchWindowException e) {
			logger.warn(" -- ERRO: elemento: '" + indice + "' NAO encontrado.");
			Assert.fail(LocalDateTime.now() + " -- NAO foi possivel trocar para a janela: '" + indice);
		} catch (Exception e) {
			logger.warn(" -- ERRO: erro ao trocar para a janela de índice: " + indice);
			Assert.fail(LocalDateTime.now() + "erro ao trocar para a janela de índice: " + indice);
		}
	}

	default void trocarJanela(String nameOrHandle, String descricaoDoPasso) {
		try {
			logger.info("----" + descricaoDoPasso);
			DriverWeb.getDriver().switchTo().window(nameOrHandle);
		} catch (NoSuchElementException e) {
			logger.warn(" -- ERRO: elemento: '" + nameOrHandle + "' NAO encontrado.");
			Assert.fail(LocalDateTime.now() + " -- NAO foi possivel trocar para a janela: '" + nameOrHandle);
		} catch (TimeoutException e) {
			logger.warn(" -- ERRO: tempo excedido para encontrar a janela: '" + nameOrHandle);
			Assert.fail(LocalDateTime.now() + " -- NAO foi possivel trocar para a janela: '" + nameOrHandle);
		} catch (ElementNotVisibleException e) {
			logger.warn(" -- ERRO: janela: '" + nameOrHandle + "' NAO esta visivel.");
			Assert.fail(LocalDateTime.now() + " -- NAO foi possivel trocar para a janela: '" + nameOrHandle);
		} catch (NoSuchWindowException e) {
			logger.warn(" -- ERRO: elemento: '" + nameOrHandle + "' NAO encontrado.");
			Assert.fail(LocalDateTime.now() + " -- NAO foi possivel trocar para a janela: '" + nameOrHandle);
		}
	}

	default void mudarAba(int numero, String descricaoDoPasso) {
		try {
			logger.info("----" + descricaoDoPasso);
			DriverWeb.getDriver().switchTo()
					.window((String) DriverWeb.getDriver().getWindowHandles().toArray()[numero]);
		} catch (NoSuchWindowException e) {
			logger.warn(" -- ERRO: aba de numero: '" + numero + "' NAO encontrado.");
			Assert.fail(LocalDateTime.now() + " -- NAO foi possivel trocar para a aba: '" + numero);
		}
	}
}